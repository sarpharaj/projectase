﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assigment1
{

    /// <summary>
    /// Created DrawTriangle class which is inherits from Shape class.
    /// This class is responsible for drawing Triangle.
    /// </summary>
    class DrawTriangle : Shape
    {
        //Decleare variable to instantiate
        public int bseValue;
        public int adjacent;
        public int hypotenuse;

        /// <summary>
        ///  constructor to initialize the instance of class
        /// </summary>
        /// <param name="x">x hold hypotenuse value provided by user</param>
        /// <param name="y">y hold base value provided by user</param>
        /// <param name="z">z hold adjacent value provided by user</param>
        public DrawTriangle(int x, int y, int z) : base(x, y)
        {
            hypotenuse = x;
            bseValue = y;
            adjacent = z;
        }

        /// <summary>
        /// This method holds specific shape(Triangle) to draw or also to fill on shape
        ///  if Fill is on
        /// </summary>
        /// <param name="mySheeting">mycaanvas holds Drawing area</param>
        public override void Draw(sheeting mySheeting)
        {
            PointF a = new Point(mySheeting.xPos, mySheeting.yPos);
            PointF b = new Point(mySheeting.xPos, mySheeting.yPos + bseValue);
            PointF c = new PointF(mySheeting.xPos + adjacent, mySheeting.yPos + bseValue);
            PointF[] pnt = { a, b, c };
            mySheeting.g.DrawPolygon(mySheeting.pen, pnt);//Draws a triangle
            if (mySheeting.fill)
            {

                mySheeting.g.FillPolygon(mySheeting.brush, pnt);//Draws a filled triangle if fill is true
            }
        }
    }
}
