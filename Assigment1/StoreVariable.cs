﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment1
{

    public class StoreVariable
    {

        Dictionary<string, int> VarDetail = new Dictionary<string, int>();


        public void StoreVar(String varName, int varValue)
        {
            VarDetail.Add(varName, varValue);
        }


        public int GetVar(String varName)
        {
            int x;
            VarDetail.TryGetValue(varName, out x);
            return x;
        }


        public void EditVar(String varName, int varValue)
        {
            VarDetail[varName] = varValue; 
        }


        public bool VarExists(String varName)
        {
            int x;
            return VarDetail.TryGetValue(varName, out x);
        }


        public void Reset()
        {
            VarDetail.Clear(); //Remove all keys and value from Dictionary
        }
    }
}
