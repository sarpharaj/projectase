﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assigment1
{

    public class sheeting
    {


        public Graphics g;


        public SolidBrush brush;


        public bool fill = false;


        public CheckUserSyntax checkSyntax;


        public StoreMethod storeMethod;


        public StoreVariable storeVariable;

        public bool error = false;


        public Pen pen;


        public int xPos, yPos;

        public sheeting(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            storeVariable = new StoreVariable();
            storeMethod = new StoreMethod();
            checkSyntax = new CheckUserSyntax();
            pen = new Pen(Color.Black, 1);//default pen with color and width
            g.DrawRectangle(pen, xPos, yPos, 1, 1);
            brush = new SolidBrush(Color.Black);
        }


        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(pen, xPos, yPos, toX, toY);//drawing line
            xPos = toX;
            yPos = toY; //pen position is moved at the end of line
        }

        public void MoveTo(int toX, int toY)
        {
            xPos = toX;
            yPos = toY;
            g.DrawRectangle(pen, xPos, yPos, 1, 1); //draw square
        }


        public void Set_Pen_Color(Color colour)
        {
            pen = new Pen(colour, 1);
            brush = new SolidBrush(colour);
        }


        public void Reset()
        {
            xPos = yPos = 0;
            pen = new Pen(Color.Black, 1);//default pen with constants
            g.Clear(SystemColors.Control);
            g.DrawRectangle(pen, xPos, yPos, 1, 1);
            storeVariable.Reset();
            storeMethod.Reset();
            error = false;
            fill = false;
        }


        public void Clear()
        {
            g.Clear(SystemColors.Control);
        }
    }
}
