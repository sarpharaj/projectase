﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment1
{
    /// <summary>
    /// Created DrawSquare class which is inherits from Shape class
    /// </summary>
    class DrawSquare : Shape
    {
        //Decleare variable to instantiate
        public int height;
        public int width;

 
        /// constructor to initialize the instance of class.
        /// </summary>
        /// <param name="x">x assign value on height</param>
        /// <param name="y">x assign value on width</param>
        public DrawSquare(int x, int y) : base(x, y)
        {
            height = x;
            width = y;
        }

        /// <summary>
        /// This method holds specific shape(Rectangle) to draw or also to fill on shape
        ///  if Fill is on
        /// </summary>
        /// <param name="mySheeting">mySheeting holds Drawing area</param>
        public override void Draw(sheeting mySheeting)
        {
            mySheeting.g.DrawRectangle(mySheeting.pen, mySheeting.xPos, mySheeting.yPos, width, height);

            //if this Expression is true i.e. fill on than Rectangle gets fill with colour provided by user 
            if (mySheeting.fill)
            {
                mySheeting.g.FillRectangle(mySheeting.brush, mySheeting.xPos, mySheeting.yPos, width, height);
            }
        }
    }
}
