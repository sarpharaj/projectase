namespace Assigment1
{
    public partial class Form1 : Form
    {
        //Bitmap to draw which will display on pictureBox
        const int bitmapX = 200;
        const int bitmapY = 500;
        public Bitmap myBitmap = new Bitmap(bitmapX, bitmapY);
        public Graphics graph;

        sheeting mysheeting;
        public Form1()
        {
            InitializeComponent();
            mysheeting = new sheeting(Graphics.FromImage(myBitmap));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ObtainInstruction();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// Read Instruction provided by the user through Command Prompt, those instruction get converted. 
        /// into Lower case and also Triming white spaces.
        /// readCommand to read single Line Instruction.
        /// readMultiCommand to read Multi  Instruction.
        /// </summary>
        public void ObtainInstruction()
        {
            String readCommand = textBox1.Text.Trim().ToLower();
            String readMultiCommand = program_Window.Text.Trim().ToLower();
            CmdSetup cmdSetup = new CmdSetup();
            cmdSetup.Command(readCommand, readMultiCommand, mysheeting);
            Refresh();
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            graph = e.Graphics;
            graph.DrawImageUnscaled(myBitmap, 0, 0);
        }

        private void aboutUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Application is about drawing a different shape using command Prompt by user\n"
               + Environment.NewLine + "Programmer Name:- Sarpharaj Alam \n"
                , "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("All COMMAND Should be Case INSENSITIVE"
            + Environment.NewLine + "======================================\n"
            + Environment.NewLine + "1. moveTo \"To move  pen position\"\nCommand-> moveTo x,y \nWhere x and y is integer value or (X,y) coordinate\n"
            + Environment.NewLine + "2. drawTo \"To Draw Line on Drawing area\"\nCommand-> drawTo x,y \nWhere x and y is integer value or (X,y) coordinate\n"
            + Environment.NewLine + "3. clear \"To Clear the Drawing area\"\nCommand-> clear\n"
            + Environment.NewLine + "4. reset \"To move pen to initial position at the top left of the Screen\"\nCommand-> reset\n"
            + Environment.NewLine + "5. run \"Read the program and Executes it\"\nCommand-> run\n"
            + Environment.NewLine + "6. rectangle \"To Draw Rectangle Shape\"\nCommand-> rectangle width,Height\n"
            + Environment.NewLine + "7. Square \"To Draw Square Shape\"\nCommand-> square width\n"
            + Environment.NewLine + "8. Circle \"To Draw Circle Shape\"\nCommand-> circle radius\n"
            + Environment.NewLine + "9. Triangle \"To Draw Triangle Shape\"\nCommand-> triangle hyp,base,adj\n"
            + Environment.NewLine + "10. Pen color \"To Choose color for drawing\"\nCommand-> pen color \n where color indicate different colour name according to user choice\n"
            + Environment.NewLine + "11. Fill on \"To fill the interior of a polygon\"\nCommand-> fill on\n"
            , "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                pictureBox1.Load(openFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.ShowDialog();
                myBitmap.Save(saveFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }
    }
}