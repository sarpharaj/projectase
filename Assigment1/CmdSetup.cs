﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Assigment1
{
    public class CmdSetup
    {
        int line = 0;

     
        public void Command(String readCommand, String readMultiCommand, sheeting mysheeting)
        {
            //Remove previous excuted program if  mysheeting get error, provide new execution area 
            if (mysheeting.error)
            {
                mysheeting.Reset();
                mysheeting.error = false;
            }

            if (readMultiCommand.Length.Equals(0))
            {

                SingleCommand(readCommand, mysheeting);   
            }
            else if (readCommand.Equals("run"))
            {
                MultiCommand(readMultiCommand, mysheeting);  
            }
            else
            {
                MultiCommand(readMultiCommand, mysheeting);    
            }
        }

     
        public void SingleCommand(String readCommand, sheeting mysheeting)
        {
            String[] readCmd = readCommand.Split(' ');
            ParameterSeperator(readCmd,  mysheeting, -1);    
        }

       
        public void MultiCommand(String readCommand, sheeting  mysheeting)
        {
            String[] value = readCommand.Split('\n');
            int num = 0;
            int x = 0;
            try
            {
                //This loop run until number of input get exists in program window
                while (num < value.Length)
                {
                    String[] readCmd = value[num].Split(' ');
                    //if user enter value and while get equal then value will be added in list data
                    if (readCmd[0].Equals("while"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endwhile"));
                        CmdLoopWhile(listData,  mysheeting, num, x);
                    }
                    
                    else if (readCmd[0].Equals("if"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endif"));
                        CmdConditionIf(listData,  mysheeting, num, x);
                    }
                    
                    else if (readCmd[0].Equals("loop"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endfor"));
                        CmdLoopFor(listData,  mysheeting, num, x);
                    }
                    
                    else if (readCmd[0].Equals("method"))
                    {
                        List<String> listData = new List<String>();
                        do
                        {
                            x++;
                            listData.Add(value[num]);
                            num++;
                            readCmd = value[num].Split(' ');
                        }
                        while (!readCmd[0].Equals("endmethod"));
                        CmdMethodSelect(listData,  mysheeting, num, x);
                    }
                    else
                    {
                        ParameterSeperator(readCmd,  mysheeting, num);    //call ParameterSeperator method
                    }
                    num++;
                }
            }
            catch
            {

            }
        }

       
        public void CmdLoopWhile(List<String> listData, sheeting  mysheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }

            try
            {
                if (readCmd[1].Split('<').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('<');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (! mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x =  mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                         mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num,  mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mysheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData,  mysheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mysheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData, mysheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split("==".ToCharArray()).Length > 1)
                {
                    String[] tempVal = readCmd[1].Split("==".ToCharArray());
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num,  mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            while (mysheeting.storeVariable.GetVar(tempVal[0]) == x)
                            {
                                newData = string.Join("\n", stringLists);
                                MultiCommand(newData, mysheeting);
                            }
                        }
                    }
                }
                else
                {
                    mysheeting.checkSyntax.ParameterCheck(false, "", num - z, mysheeting, line);
                    line = line + 20;
                }
            }
            catch
            {
                mysheeting.checkSyntax.ParameterCheck(false, "", num - z,  mysheeting, line);
                line = line + 20;
            }

        }

        
        public void CmdConditionIf(List<String> listData, sheeting mysheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringList = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringList.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (readCmd[1].Split('<').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('<');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mysheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData,  mysheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mysheeting.storeVariable.GetVar(tempVal[0]) < x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData,  mysheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split('>').Length > 1)
                {
                    String[] tempVal = readCmd[1].Split('>');
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mysheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mysheeting);
                            }
                        }
                    }
                }
                else if (readCmd[1].Split("==".ToCharArray()).Length > 1)
                {
                    String[] tempVal = readCmd[1].Split("==".ToCharArray());
                    if (!int.TryParse(tempVal[1], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[1]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(tempVal[1]);
                        }
                    }
                    if (isValueExists)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, tempVal[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    else
                    {
                        if (!mysheeting.storeVariable.VarExists(tempVal[0]))
                        {
                            isValueExists = true;
                        }
                        if (isValueExists)
                        {
                            mysheeting.checkSyntax.ParameterCheck(false, tempVal[0], num, mysheeting, line);
                            line = line + 20;
                        }
                        else
                        {
                            if (mysheeting.storeVariable.GetVar(tempVal[0]) > x)
                            {
                                newData = string.Join("\n", stringList);
                                MultiCommand(newData, mysheeting);
                            }
                        }
                    }
                }
                else
                {
                    mysheeting.checkSyntax.ParameterCheck(false, "", num - z, mysheeting, line);
                    line = line + 20;
                }
            }
            catch
            {
                mysheeting.checkSyntax.ParameterCheck(false, "", num - z, mysheeting, line);
                line = line + 20;
            }
        }

        
        public void CmdLoopFor(List<String> listData, sheeting mysheeting, int num, int z)//LoopFor
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            int x = 0;
            bool isValueExists = false;
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (readCmd[1].Equals("for"))
                {
                    if (!int.TryParse(readCmd[2], out x))
                    {
                        if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                        {
                            isValueExists = true;
                        }
                        else
                        {
                            x = mysheeting.storeVariable.GetVar(readCmd[2]);
                        }
                    }
                }
                if (isValueExists)
                {
                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                    line = line + 20;
                }
                else
                {
                    for (int b = 0; b < x; b++)
                    {
                        newData = string.Join("\n", stringLists);
                        MultiCommand(newData, mysheeting);
                    }
                }
            }
            catch
            {
                mysheeting.checkSyntax.ParameterCheck(false, "", num - z, mysheeting, line);
                line = line + 20;
            }
        }

      
        public void CmdMethodSelect(List<String> listData, sheeting mysheeting, int num, int z)
        {
            string newData = string.Join("\n", listData);
            String[] value = newData.Split('\n');
            String[] readCmd = value[0].Split(' ');
            String x = null;
            bool isValueExists = false;
            String[] method = readCmd[1].Split('(', ')');
            String[] methodValue = null;

            mysheeting.storeMethod.StoreVar(method[0], method[1]);
            List<String> stringLists = new List<string>();
            int allValue = 1;
            while (allValue < value.Length)
            {
                stringLists.Add(value[allValue]);
                allValue++;
            }
            try
            {
                if (mysheeting.storeMethod.VarExists(method[0]))
                {
                    x = mysheeting.storeMethod.GetVar(method[0]);
                    methodValue = x.Split(',');
                }
                else
                {
                    isValueExists = true;
                }
                if (isValueExists)
                {
                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[1], num,  mysheeting, line);
                    line = line + 20;
                }
                else
                {
                    newData = string.Join("\n", stringLists);
                    String methodCmd = method[0] + "command";
                    mysheeting.storeMethod.StoreVar(methodCmd, newData);
                }

            }
            catch
            {
                mysheeting.checkSyntax.ParameterCheck(false, "", num - z, mysheeting, line);
                line = line + 20;
            }

        }

       
        public void ParameterSeperator(String[] readCmd, sheeting mysheeting, int num)
        {
            try
            {
                String[] method = readCmd[0].Split('(', ')');
                if (readCmd[0].Equals("drawto"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isVarExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[0]))
                            {
                                isVarExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isVarExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[0], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (! mysheeting.storeVariable.VarExists(data[1]))
                            {
                                isVarExists = true;
                            }
                            else
                            {
                                y = mysheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isVarExists)
                            {
                             
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        mysheeting.DrawLine(x, y);
                    }
                }

                else if (readCmd[0].Equals("moveto"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[0], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mysheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[1], num, mysheeting, line);
                                num = num + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        mysheeting.MoveTo(x, y);
                    }
                }

                else if (readCmd[0].Equals("rectangle"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[0], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mysheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[1], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        Shape drawRectangle = new DrawSquare(x, y);
                        drawRectangle.Draw(mysheeting);
                    }
                }
                else if (readCmd[0].Equals("square"))
                {
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(readCmd[1], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(readCmd[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(readCmd[1]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        Shape drawSquare = new DrawSquare(x, x);
                        drawSquare.Draw(mysheeting);
                    }
                }

                else if (readCmd[0].Equals("triangle"))
                {
                    String[] data = readCmd[1].Split(','); ;
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(data[0], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[0]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(data[0]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[0], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[1], out y))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                y = mysheeting.storeVariable.GetVar(data[1]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[1], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                        if (!int.TryParse(data[2], out z))
                        {
                            if (!mysheeting.storeVariable.VarExists(data[2]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                z = mysheeting.storeVariable.GetVar(data[2]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, data[2], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        Shape drawTriangle = new DrawTriangle(x, y, z);
                        drawTriangle.Draw( mysheeting);
                    }
                }
                else if (readCmd[0].Equals("circle"))
                {
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        if (!int.TryParse(readCmd[1], out x))
                        {
                            if (!mysheeting.storeVariable.VarExists(readCmd[1]))
                            {
                                isValueExists = true;
                            }
                            else
                            {
                                x = mysheeting.storeVariable.GetVar(readCmd[1]);
                            }
                            if (isValueExists)
                            {
                                mysheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mysheeting, line);
                                line = line + 20;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        Shape drawCircle = new DrawCircle(x);
                        drawCircle.Draw(mysheeting);
                    }
                }
                else if (readCmd[0].Equals("polygon"))
                {
                    String[] data = readCmd[1].Split(',');
                    List<int> listPoints = new List<int>();
                    int i = 0;
                    int x = 0;
                    bool isValueExists = false;
                    try
                    {
                        while (data.Length > i)
                        {
                            if (!int.TryParse(data[i], out x))
                            {
                                if (!mysheeting.storeVariable.VarExists(data[i]))
                                {
                                    isValueExists = true;
                                }
                                else
                                {
                                    listPoints.Add(mysheeting.storeVariable.GetVar(data[i]));
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, data[i], num,  mysheeting, line);
                                    line = line + 20;
                                }
                            }
                            listPoints.Add(x);
                            i++;
                        }
                    }
                    catch (Exception e)
                    {
                        mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        int[] polyArrays = listPoints.ToArray();
                        
                    }
                }
                else if (readCmd[0].Equals("pen"))
                {
                    Color color = Color.FromName(readCmd[1]); //User input color 

                    if (color.IsKnownColor == false)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mysheeting, line);
                        line = line + 20;
                    }
                    if (!mysheeting.error)
                    {
                        mysheeting.Set_Pen_Color(color); 
                    }
                }
                else if (readCmd[0].Equals("fill"))
                {
                    bool fillOn = readCmd[1].Equals("on");
                    bool fillOff = readCmd[1].Equals("off");

                    if (fillOn == false && fillOff == false)
                    {
                        mysheeting.checkSyntax.ParameterCheck(false, readCmd[1], num, mysheeting, line);
                        line = line + 20;
                    }

                    if (!mysheeting.error)
                    {
                        if (fillOn)
                        {
                            mysheeting.fill = true;
                        }
                        else if (fillOff)
                        {
                            mysheeting.fill = false;
                        }
                    }
                }
                else if (readCmd[0].Equals("clear"))
                {
                    if (!mysheeting.error)
                    {
                        mysheeting.Clear();
                    }
                }
                else if (readCmd[0].Equals("reset"))
                {
                    if (!mysheeting.error)
                    {
                        mysheeting.Reset();
                    }
                }
                else if (readCmd[0].Equals("exit"))
                {
                    if (!mysheeting.error)
                    {
                        Application.Exit();
                    }
                }
                else if (mysheeting.storeMethod.VarExists(method[0]))
                {
                    String[] methodValue = (mysheeting.storeMethod.GetVar(method[0])).Split(',');
                    String methodCmd = method[0] + "command";
                    String methodCommand = mysheeting.storeMethod.GetVar(methodCmd);
                    String[] userValue = method[1].Split(',');
                    int x = 0;
                    while (methodValue.Length > x)
                    {
                        String[] valueStore = (methodValue[x] + " = " + userValue[x]).Split(' ');
                        ParameterSeperator(valueStore, mysheeting, num);
                        x++;
                    }
                    MultiCommand(methodCommand, mysheeting);

                }
                else if (readCmd[1].Equals("="))
                {
                    try
                    {
                        if (readCmd[3].Equals("+"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mysheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mysheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mysheeting.checkSyntax.ParameterCheck(e, num,  mysheeting, line);
                                line = line + 20;
                            }
                            varValue = x + y;
                            mysheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("-"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mysheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mysheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                                line = line + 20;
                            }
                            varValue = x - y;
                            mysheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("/"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mysheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mysheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                                line = line + 20;
                            }
                            varValue = x / y;
                            mysheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                        if (readCmd[3].Equals("*"))
                        {
                            int varValue;
                            int x = 0;
                            int y = 0;
                            bool isValueExists = false;
                            try
                            {
                                if (!int.TryParse(readCmd[2], out x))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        x = mysheeting.storeVariable.GetVar(readCmd[2]);
                                    }
                                }

                                if (!int.TryParse(readCmd[4], out y))
                                {
                                    if (!mysheeting.storeVariable.VarExists(readCmd[4]))
                                    {
                                        isValueExists = true;
                                    }
                                    else
                                    {
                                        y = mysheeting.storeVariable.GetVar(readCmd[4]);
                                    }
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                                    line = line + 20;
                                }
                            }
                            catch (Exception e)
                            {
                                mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                                line = line + 20;
                            }
                            varValue = x * y;
                            mysheeting.storeVariable.EditVar(readCmd[0], varValue);
                        }
                    }
                    catch
                    {
                        int x = 0;
                        try
                        {
                            bool isValueExists = false;
                            if (!int.TryParse(readCmd[2], out x))
                            {
                                if (!mysheeting.storeVariable.VarExists(readCmd[2]))
                                {
                                    isValueExists = true;
                                }
                                else
                                {
                                    x = mysheeting.storeVariable.GetVar(readCmd[2]);
                                }
                                if (isValueExists)
                                {
                                    mysheeting.checkSyntax.ParameterCheck(false, readCmd[2], num, mysheeting, line);
                                    line = line + 20;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            mysheeting.checkSyntax.ParameterCheck(e, num, mysheeting, line);
                            line = line + 20;
                        }
                        if (!mysheeting.error)
                        {
                            if (!mysheeting.storeVariable.VarExists(readCmd[0]))
                            {
                                mysheeting.storeVariable.StoreVar(readCmd[0], x);
                            }
                            else
                            {
                                mysheeting.storeVariable.EditVar(readCmd[0], x);
                            }
                        }
                    }
                }
                else
                {
                    mysheeting.checkSyntax.CommandCheck( mysheeting, num, line);
                    line = line + 20;
                }
            }
            catch
            {
                 mysheeting.checkSyntax.CommandCheck( mysheeting, num, line);
                line = line + 20;
            }

        }
    }
}