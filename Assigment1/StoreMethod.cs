﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment1
{

    public class StoreMethod
    {
        Dictionary<string, string> VarDetail = new Dictionary<string, string>();


        public void StoreVar(String methodName, String methodValue)
        {
            VarDetail.Add(methodName, methodValue);
        }


        public String GetVar(String methodName)
        {
            String x;
            VarDetail.TryGetValue(methodName, out x);
            return x;
        }


        public bool VarExists(String methodName)
        {
            String x;
            return VarDetail.TryGetValue(methodName, out x);
        }


        public void Reset()
        {
            VarDetail.Clear();
        }
    }
}

