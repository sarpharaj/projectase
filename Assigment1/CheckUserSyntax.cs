﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Assigment1
{

    public class CheckUserSyntax
    {
        public void CommandCheck(sheeting mysheeting, int num, int x)
        {
            Font errortxtFont = new Font("Arial", 10);

            SolidBrush errortxtBrush = new SolidBrush(Color.Black);
            num++;
            if (num != 0)
            {
                if (x == 0)
                {

                    mysheeting.Reset();
                }

                mysheeting.g.DrawString("Command on line " + (num) + " does not exist", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {

                mysheeting.g.DrawString("Command does not exist", errortxtFont, errortxtBrush, 0, 0);
            }

            mysheeting.error = true;

        }



        public void ParameterCheck(bool parameter, String data, int num, sheeting mysheeting, int x)
        {

            if (!parameter)
            {
                Font errortxtFont = new Font("Arial", 10);
                SolidBrush errortxtBrush = new SolidBrush(Color.Black);
                if (x == 0)
                {
                    mysheeting.Reset();
                }
                if ((num + 1) == 0)
                {

                    mysheeting.g.DrawString("Paramater " + data + " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }
                else
                {

                    mysheeting.g.DrawString("Paramater " + data + " on line " + (num + 1) + " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }

                mysheeting.error = true;
            }
        }


        public void ParameterCheck(Exception e, int num, sheeting mysheeting, int x)
        {
            Font errortxtFont = new Font("Arial", 10);
            SolidBrush errortxtBrush = new SolidBrush(Color.Black);

            if (x == 0)
            {

                mysheeting.Reset();
            }
            if ((num + 1) == 0)
            {

                mysheeting.g.DrawString("Wrong number of parameters inputted", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {

                mysheeting.g.DrawString("Wrong number of parameters inputted on line" + (num + 1), errortxtFont, errortxtBrush, 0, 0 + x);
            }

            mysheeting.error = true;
        }

    }
}

