﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Assigment1
{

    /// <summary>
    ///  Created DrawCircle class which is inherits from Shape class.
    /// This class hold method and properties for Drawing Circle
    /// </summary>
    class DrawCircle : Shape
    {
        //Decleare variable to instantiate
        public int radius;

        /// <summary>
        ///  constructor to initialize the instance of class
        /// </summary>
        /// <param name="r">r holds radius of circle</param>
        public DrawCircle(int r) : base(r, 0)
        {
            radius = r;
        }

        /// <summary>
        /// This method holds specific shape(Circle) to draw or also to fill on shape
        ///  if Fill is on
        /// </summary>
        /// <param name="mySheeting">mycaanvas holds Drawing area</param>
        public override void Draw(sheeting mySheeting)
        {
            mySheeting.g.DrawEllipse(mySheeting.pen, mySheeting.xPos, mySheeting.yPos, (radius * 2), (radius * 2));

            //if this Expression is true i.e. fill on than circle gets fill with colour provided by user 
            if (mySheeting.fill)
            {
                mySheeting.g.FillEllipse(mySheeting.brush, mySheeting.xPos, mySheeting.yPos, (radius * 2), (radius * 2));
            }
        }


    }
}
